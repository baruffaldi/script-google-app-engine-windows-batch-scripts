@echo off
set BASEURL="."
echo GPRESS-UPLOADER: Updating Indexes
appcfg.py update_indexes %BASEURL%\src
echo GPRESS-UPLOADER: Clear Unused Indexes
appcfg.py vacuum_indexes %BASEURL%\src
pause