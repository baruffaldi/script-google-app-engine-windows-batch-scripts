@echo off
echo GPRESS-UPLOADER: Clear Unused Indexes
appcfg.py vacuum_indexes .
echo GPRESS-UPLOADER: Updating Indexes
appcfg.py update_indexes .
pause