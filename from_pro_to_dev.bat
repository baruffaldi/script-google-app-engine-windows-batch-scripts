@echo off
set BASEURL="."
set ALL_KINDS=1
set KINDS=Settings,Address,Page,PageCategory,PageDetails
rem set KINDS=Settings,SettingsHistory,Address,Page,PageCategory

rem TODO: Visualizzazione Step sul numero di kinds da lavorare

set PROJECT_ID=main-agency-contabilita
set DEV_PROJECT_ID=%PROJECT_ID%
rem set DEV_PROJECT_ID=main-agency-contabilita
set DOMAIN=main-agency-contabilita.appspot.com
rem set DOMAIN=%PROJECT_ID%.appspot.com
set G_DEBUG=0

set URL=http://%DOMAIN%/_ah/remote_api
set DEV_URL=http://localhost:8085/_ah/remote_api
set NTHREADS=4
set S_DIR=%BASEURL%\src
set L_DIR=%BASEURL%\logs
set DB_DIR=%BASEURL%\sql3
set D_DIR=%BASEURL%\data

for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set yyyy=%%c
for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set mm=%%a
for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set dd=%%b
for /f "tokens=1 delims=: " %%h in ('time /T') do set hh=%%h
for /f "tokens=2 delims=: " %%m in ('time /T') do set mn=%%m
for /f "tokens=3 delims=: " %%a in ('time /T') do set ss=%%a
SET now=%mm%%dd%%yyyy%.%hh%%mn%%ss%

set DB_FILENAME=%DB_DIR%\bulkloader-progress-%now%.sql3
set RESULT_DB_FILENAME=%DB_DIR%\bulkloader-results-%now%.sql3
set LOG_FILE=%L_DIR%\bulkloader-log-%now%.log

For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set DATA=%%a%%b)
set APP_ID=s~%PROJECT_ID%
set DEV_APP_ID=dev~%DEV_PROJECT_ID%
set BULKYAML=%BASEURL%\bulkloader.yaml
if not exist %BULKYAML% (
	echo GPRESS-UPLOADER: Creating bulkloader.yaml for application %APP_ID%
	appcfg.py --application="%APP_ID%" --filename="%BULKYAML%" --url="%URL%" create_bulkloader_config %S_DIR%
	if %G_DEBUG% == 1 pause
)



if %ALL_KINDS% == 0 call :parse "%KINDS%"
if %ALL_KINDS% == 1 call :notparse "%KINDS%"
goto :end

:notparse
setlocal
set FILENAME=%BASEURL%\data\data.pro.%PROJECT_ID%.sqlite
if not exist %FILENAME% (
	echo GPRESS-UPLOADER: Downloading data from production application %APP_ID%
	appcfg.py --db_filename=%DB_FILENAME% --result_db_filename=%RESULT_DB_FILENAME% --log_file=%LOG_FILE% --application="%APP_ID%" --filename="%FILENAME%" --url="%URL%" download_data %S_DIR%
	if %G_DEBUG% == 1 pause
)
if exist %BULKYAML% (
	if exist %FILENAME% (
		echo GPRESS-UPLOADER: Uploading %APP_ID% data to development server
		appcfg.py --db_filename=%DB_FILENAME% --result_db_filename=%RESULT_DB_FILENAME% --log_file=%LOG_FILE% --num_threads=%NTHREADS% --application="%DEV_APP_ID%" --filename="%FILENAME%" --url="%DEV_URL%" upload_data %S_DIR%
		if %G_DEBUG% == 1 pause
	)
)
goto :end
endlocal
exit /b

:parse
setlocal
set list=%1
set list=%list:"=%
FOR /f "tokens=1* delims=," %%a IN ("%list%") DO (
  if not "%%a" == "" call :sub %%a
  if not "%%b" == "" call :parse "%%b"
)
endlocal
exit /b

:sub
setlocal
	set FILENAME=%BASEURL%\data\data.pro.%PROJECT_ID%.%1.sqlite
	if not exist %FILENAME% (
		echo GPRESS-UPLOADER: Downloading %1 data from production application %APP_ID%
		appcfg.py --db_filename=%DB_FILENAME% --result_db_filename=%RESULT_DB_FILENAME% --log_file=%LOG_FILE% --kind="%1" --application="%APP_ID%" --config_file="%BULKYAML%" --filename="%FILENAME%" --url="%URL%" download_data %S_DIR%
		if %G_DEBUG% == 1 pause
	)
	if exist %BULKYAML% (
		if exist %FILENAME% (
			echo GPRESS-UPLOADER: Uploading %APP_ID% %1 data to development server
			appcfg.py --db_filename=%DB_FILENAME% --result_db_filename=%RESULT_DB_FILENAME% --log_file=%LOG_FILE% --kind="%1" --num_threads=%NTHREADS% --config_file="%BULKYAML%" --application="%DEV_APP_ID%" --filename="%FILENAME%" --url="%DEV_URL%" upload_data %S_DIR%
			if %G_DEBUG% == 1 pause
		)
	)
endlocal
exit /b

:end
echo Finished!
pause
echo Closing...